﻿using Autofac;
using BDDDemo.Implementation;
using BDDDemo.Library;

namespace BDDDemo.Config
{
    public class AutofacConfig
    {

        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UserRepository>().As<IUserProvider>().SingleInstance();
            builder.RegisterType<LoanRepository>().As<ILoanProvider>().SingleInstance(); ;

            return builder.Build();
        }
       
    }
}
