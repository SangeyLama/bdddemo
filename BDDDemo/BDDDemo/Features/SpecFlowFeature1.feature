﻿Feature: You can only lend 5 books at a time
	As a librarian 
	I want to ensure members only up to 5 books at a time
	In order to avoid too many books being loaned by 1 member

Scenario: Customer tries to check out a book when they already have 4 checked out
	Given I am a member at the library with the id 1
	And I have loaned out 4 books
	When I try to check out a new book
	Then I should get a confirmation of the loan
	And my account should have 5 books on loan

Scenario: Customer tries to check out a book when they already have 5 checked out
	Given I am a member at the library with the id 1
	And I have loaned out 5 books
	When I try to check out a new book
	Then I should get a rejection of the loan
	And my account should have 5 books on loan

Scenario: Customer tries to check out a book when they have no books checked out
	Given I am a member at the library with the id 1
	And I have loaned out 0 books
	When I try to check out a new book
	Then I should get a confirmation of the loan
	And my account should have 1 books on loan

