﻿using BDDDemo.Library;

namespace BDDDemo.Implementation
{
    public class LoanRepository : ILoanProvider
    {
        public int getNumberOfActiveLoans(string memberName)
        {
            if (memberName.Equals("Sangey"))
                return 4;
            else
                return 0;
        }
    }
}
