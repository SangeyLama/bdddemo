﻿using BDDDemo.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDDDemo.Implementation
{
    public class UserRepository : IUserProvider
    {
        public string GetMember(int id)
        {
            if (id == 1)
                return "Sangey";
            else
                return "";
        }
    }
}
