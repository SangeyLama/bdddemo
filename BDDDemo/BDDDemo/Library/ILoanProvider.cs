﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDDDemo.Library
{
    public interface ILoanProvider
    {
        int getNumberOfActiveLoans(string memberName);
    }
}
