﻿namespace BDDDemo.Library
{
    public interface IUserProvider
    {
        string GetMember(int id);
    }
}
