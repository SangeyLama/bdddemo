﻿using Autofac;
using BDDDemo.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDDDemo
{  
    class Program
    {
        private static IContainer _container { get; set; }

        static void Main(string[] args)
        {
            _container = AutofacConfig.BuildContainer();

        }
    }
}
