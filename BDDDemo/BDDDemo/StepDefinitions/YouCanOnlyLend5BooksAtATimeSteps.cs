﻿using Autofac;
using BDDDemo.Config;
using BDDDemo.Library;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using Moq;


namespace BDDDemo
{
    [Binding]
    public class YouCanOnlyLend5BooksAtATimeSteps
    {
        private Mock<IUserProvider> _userRepository;
        private Mock<ILoanProvider> _loanRepository;
        private IContainer _container;
        private string _currentMember;
        private int _noOfLoanedBooks;

        public YouCanOnlyLend5BooksAtATimeSteps()
        {
            _container = AutofacConfig.BuildContainer();
            _userRepository = new Mock<IUserProvider>();
            _loanRepository = new Mock<ILoanProvider>();

            _userRepository.Setup(u => u.GetMember(2)).Returns("");
            _userRepository.Setup(u => u.GetMember(1)).Returns("Sangey");
        }

        [Given(@"I am a member at the library with the id (.*)")]
        public void GivenIAmAMemberAtTheLibrary(int p0)
        {
            var user = _userRepository.Object.GetMember(p0);
            if(user != null)
            {
                _currentMember = user;
            }

            Assert.IsNotNull(user);
        }
        
        [Given(@"I have loaned out (.*) books")]
        public void GivenIHaveLoanedOutBooks(int p0)
        {
            _loanRepository.Setup(l => l.getNumberOfActiveLoans("Sangey")).Returns(p0);
            int numberOfLoans = _loanRepository.Object.getNumberOfActiveLoans(_currentMember);
            _noOfLoanedBooks = numberOfLoans;

            Assert.AreEqual(p0, numberOfLoans);
        }
        
        [When(@"I try to check out a new book")]
        public void WhenITryToCheckOutANewBook()
        {

        }
        
        [Then(@"I should get a (.*) of the loan")]
        public void ThenIShouldGetAStatusOfTheLoan(string p0)
        {
            if (p0.Equals("confirmation"))
            {
                doSomething();
            }
            if (p0.Equals("rejection"))
            {
                doSomethingElse();
            }
        }

        private void doSomethingElse()
        {
            
        }

        private void doSomething()
        {
            _noOfLoanedBooks++;
        }

        [Then(@"my account should have (.*) books on loan")]
        public void ThenMyAccountShouldHaveBooksOnLoan(int p0)
        {
            int noOfLoanedBooks = _noOfLoanedBooks;
       
            Assert.AreEqual(p0, noOfLoanedBooks);
        }
    }
}
