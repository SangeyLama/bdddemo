﻿using LibraryProjectG8.API.Interfaces;
using LibraryProjectG8.Controllers;
using LibraryProjectG8.Controllers.Interfaces;
using LibraryProjectG8.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LibraryProject.UnitTests
{
    [TestClass]
    public class BookControllerTests
    {
        private IBookController _target;
        private Mock<IGoodReadsService> _goodReadsService;

        [TestInitialize]
        public void SetUp()
        {
            _goodReadsService = new Mock<IGoodReadsService>();
            var bookDune = new Book { Id = "1", Title = "Dune", Description = "If you walk without Rhythm, you won't attract the worm" };
            var bookHarryPotter = new Book { Id = "2", Title = "Harry Potter", Description = "Wizard Jock tries to take over the world" };
            var bookLordOfTheRings = new Book { Id = "3", Title = "Lord Of The Rings", Description = "Midgets fuck up dark lords plan" };
            _goodReadsService.Setup(b => b.GetBook("Dune")).Returns(new GoodreadsResponse { Book = bookDune });
            _goodReadsService.Setup(b => b.GetBook("Harry Potter")).Returns(new GoodreadsResponse { Book = bookHarryPotter });
            _goodReadsService.Setup(b => b.GetBook("Lord Of The Rings")).Returns(new GoodreadsResponse { Book = bookLordOfTheRings });

            _target = new BookController(_goodReadsService.Object);
            
        }

        [TestMethod]
        [DataRow("Dune", "1", DisplayName = "Dune Test")]
        [DataRow("Harry Potter", "2", DisplayName = "Harry Potter Test")]
        [DataRow("Lord Of The Rings", "3", DisplayName = "Lord Of The Rings Test")]
        public void GetBookInfo_SearchTitle_GetTitleInfo(string title, string expectedId)
        {
            var bookInfo = _target.GetBookInfo(title);

            Assert.AreEqual(title, bookInfo.Title);
            Assert.AreEqual(expectedId, bookInfo.Id);
        }
    }
}
