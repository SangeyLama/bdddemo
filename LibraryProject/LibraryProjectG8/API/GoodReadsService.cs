﻿using LibraryProjectG8.API.Interfaces;
using LibraryProjectG8.DTOs;
using RestSharp;

namespace LibraryProjectG8.API
{
    public class GoodReadsService : IGoodReadsService
    {
        private RestClient client;
        private string baseRequest;

        public GoodReadsService()
        {
            client = new RestClient
            {
                BaseUrl = new System.Uri("https://www.goodreads.com/")
            };
            baseRequest = "book/title.xml?&key=CuKLdTJfnOutZbub5P7fA&title=";
        }

        public GoodreadsResponse GetBook(string query)
        {
            var requestString = baseRequest + query;
            var request = new RestRequest(requestString, Method.GET);
            return client.Execute<GoodreadsResponse>(request).Data;
        }
    }
}
