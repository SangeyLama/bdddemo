﻿using LibraryProjectG8.DTOs;

namespace LibraryProjectG8.API.Interfaces
{
    public interface IGoodReadsService
    {
        GoodreadsResponse GetBook(string query);
    }
}
