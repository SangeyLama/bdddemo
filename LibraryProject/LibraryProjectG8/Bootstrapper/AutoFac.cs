﻿using Autofac;
using LibraryProjectG8.API;
using LibraryProjectG8.API.Interfaces;
using LibraryProjectG8.Controllers;
using LibraryProjectG8.Controllers.Interfaces;

namespace LibraryProjectG8.Bootstrapper
{
    public class AutoFac
    {
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<GoodReadsService>().As<IGoodReadsService>().SingleInstance();
            builder.RegisterType<BookController>().As<IBookController>().SingleInstance();

            return builder.Build();
        }
    }
}
