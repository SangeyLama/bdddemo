﻿using Autofac;
using LibraryProjectG8.Controllers.Interfaces;

namespace LibraryProjectG8.Bootstrapper
{
    public class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            Container = AutoFac.BuildContainer();

            var bookCtr = Container.Resolve<IBookController>();

            while (true)
            {
                System.Console.WriteLine("Enter Title: ");
                var input = System.Console.ReadLine();
                var bookInfo = bookCtr.GetBookInfo(input);
                System.Console.WriteLine(bookInfo.Id);
                System.Console.WriteLine(bookInfo.Title);
                System.Console.WriteLine(bookInfo.Description);
                System.Console.WriteLine("--------------------------- \n");                
            }
        }

    }
}
