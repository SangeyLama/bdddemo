﻿using LibraryProjectG8.API.Interfaces;
using LibraryProjectG8.Controllers.Interfaces;
using LibraryProjectG8.Mappers;
using LibraryProjectG8.Models;

namespace LibraryProjectG8.Controllers
{
    public class BookController : IBookController
    {
        private IGoodReadsService goodReadsService;
        public BookController(IGoodReadsService goodReadsService)
        {
            this.goodReadsService = goodReadsService;
        }

        public BookInfo GetBookInfo(string title)
        {
            var bookInfo = new BookInfo { Title = ""};

            var response = goodReadsService.GetBook(title);
            if (response.Book != null)
            {
                bookInfo = BookMapper.FromDTO(response.Book);
            }

            return bookInfo;
        }
    }
}
