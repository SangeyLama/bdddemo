﻿using LibraryProjectG8.Models;

namespace LibraryProjectG8.Controllers.Interfaces
{
    public interface IBookController
    {
        BookInfo GetBookInfo(string title);
    }
}
