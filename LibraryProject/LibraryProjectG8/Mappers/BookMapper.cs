﻿using LibraryProjectG8.DTOs;
using LibraryProjectG8.Models;

namespace LibraryProjectG8.Mappers
{
    public class BookMapper
    {
        public static BookInfo FromDTO(Book book)
        {
            return new BookInfo
            {
                Id = book.Id,
                Description = book.Description,
                Title = book.Title
            };
        }
    }
}
