﻿namespace LibraryProjectG8.Models
{
    public class BookInfo
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
